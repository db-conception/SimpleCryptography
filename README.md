# SimpleCryptography

POC of simples cryptograhic functions. 
Source : https://docs.microsoft.com/fr-fr/dotnet/standard/security/cryptographic-services#public-key-encryption 

![Screenshot](/res/console.png)

## Symmetric cryptography (Secret-Key encryption)
```csharp
string message = "my message to encrypt";
Console.WriteLine("Message :			" + message);
//encrypt
byte[] key = Encoding.UTF8.GetBytes("my key for encryption. I need 32"); //... byte
byte[] IV = Encoding.UTF8.GetBytes("my IV for encryp"); //...tion (need 16 byte)
//To generate dynamic key and IV, I should use :
//using (AesManaged myAes = new AesManaged()) { var key = myAes.Key; var IV = myAes.IV; }
var encryptedMessage = SymmetricCryptography.EncryptStringToBytes_Aes(message, key, IV);
//not really made to be convert as string
Console.WriteLine("Encrypted message :		" + Encoding.UTF8.GetString(encryptedMessage));
//decrypt
var decryptedMessage = SymmetricCryptography.DecryptStringFromBytes_Aes(encryptedMessage, key, IV);
Console.WriteLine("Decrypted message :		" + decryptedMessage);
```

## Asymmetric cryptography (Public-Key encryption)
```csharp
var message = "my async message to encrypt";
Console.WriteLine("Message :			" + message);
Console.WriteLine();
using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
{
    //RSA.ExportParameters(false) = the public key
    var publickey = RSA.ExportParameters(false);
    // the RSA.ExportParameters(true) = the private key
    var privatekey = RSA.ExportParameters(true);

    //encrypt
    var encryptedMessage = AsymmetricCryptography.RSAEncrypt(Encoding.UTF8.GetBytes(message), publickey, false);
    //not really made tp be convert as string
    Console.WriteLine("Encrypted message :		" + Encoding.UTF8.GetString(encryptedMessage));
    //decrypt
    var decryptedMessage = Encoding.UTF8.GetString(AsymmetricCryptography.RSADecrypt(encryptedMessage, privatekey, false));
    Console.WriteLine("Decrypted message :		" + decryptedMessage);
}
```